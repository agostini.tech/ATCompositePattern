//
//  Pilot.swift
//  ATCompositePattern
//
//  Created by Dejan on 13/06/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

class Pilot: Player
{
    var name: String
    var kills: Int
    
    init(name: String, kills: Int) {
        self.name = name
        self.kills = kills
    }
    
    func move(x: Int, y: Int) {
        print("Pilot '\(self.name)' flying to [\(x):\(y)]")
    }
    
    func shoot(x: Int, y: Int) {
        self.move(x: x, y: y)
        print("Pilot '\(self.name)' dropping bombs at [\(x):\(y)]")
        
        if x == y { // Some arbitrary piece of logic so we don't kill every time we shoot.
            self.kills += 1
        }
    }
    
    func addPlayer(_ player: Player) {
        print("[\(self.name)]: I don't have sufficient rank to command anyone :(")
    }
    
    func removePlayer(_ player: Player) {
        print("[\(self.name)]: I would love to remove my boss, but I can't!")
    }
    
    func find(_ name: String) -> Player? {
        if name == self.name {
            return self
        } else {
            return nil
        }
    }
}
