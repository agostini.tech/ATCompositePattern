//
//  Player.swift
//  ATCompositePattern
//
//  Created by Dejan on 13/06/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol Player
{
    var name: String { get }
    var kills: Int { get }
    
    func move(x: Int, y: Int)
    func shoot(x: Int, y: Int)
    
    func addPlayer(_ player: Player)
    func removePlayer( _ player: Player)
    
    func find(_ name: String) -> Player?
}
