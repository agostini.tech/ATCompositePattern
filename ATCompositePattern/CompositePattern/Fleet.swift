//
//  Fleet.swift
//  ATCompositePattern
//
//  Created by Dejan on 14/06/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

class Fleet
{
    var name: String
    var fleetCommander: Player?
    
    init(name: String) {
        self.name = name
        self.fleetCommander = createFleetCommander()
    }
    
    private func createFleetCommander() -> Player
    {
        let redSquad = Commander(name: "Sentinel[SC]")
        redSquad.addPlayer(Pilot(name: "Ripper", kills: 44))
        redSquad.addPlayer(Pilot(name: "FluffyBunny", kills: 8876))
        redSquad.addPlayer(Pilot(name: "Avenger", kills: 56))
        
        let blueSquad = Commander(name: "!crazy[SC]")
        blueSquad.addPlayer(Pilot(name: "Daisy", kills: 888))
        blueSquad.addPlayer(Pilot(name: "SafeApproach", kills: 9855))
        
        let leftWing = Commander(name: "Redeemer[WC]")
        leftWing.addPlayer(redSquad)
        leftWing.addPlayer(blueSquad)
        leftWing.addPlayer(Pilot(name: "Recon1", kills: 2))
        leftWing.addPlayer(Pilot(name: "Guardian", kills: 66))
        
        let orangeSquad = Commander(name: "PrettyPurple[SC]")
        orangeSquad.addPlayer(Pilot(name: "ComeCloser", kills: 9956))
        orangeSquad.addPlayer(Pilot(name: "N00bGone", kills: 588))
        
        let greenSquad = Commander(name: "TheGardener[SC]")
        greenSquad.addPlayer(Pilot(name: "MammaMia", kills: 8875))
        greenSquad.addPlayer(Pilot(name: "RedRooster", kills: 421))
        
        let rightWing = Commander(name: "Goldsnake[WC]")
        rightWing.addPlayer(orangeSquad)
        rightWing.addPlayer(greenSquad)
        rightWing.addPlayer(Pilot(name: "Logi", kills: 42))
        
        let fleetCommander = Commander(name: "Ramzes[FC]")
        fleetCommander.addPlayer(leftWing)
        fleetCommander.addPlayer(rightWing)
        fleetCommander.addPlayer(Pilot(name: "Praetorian", kills: 76))
        fleetCommander.addPlayer(Pilot(name: "Assassin", kills: 55))
        
        return fleetCommander
    }
    
    func moveFleet(x: Int, y: Int) {
        self.fleetCommander?.move(x: x, y: y)
    }
    
    func fleetFire(x: Int, y: Int) {
        self.fleetCommander?.shoot(x: x, y: y)
    }
    
    func move(playerName name: String, x: Int, y: Int) {
        self.fleetCommander?.find(name)?.move(x: x, y: y)
    }
    
    func fire(playerName name: String, x: Int, y: Int) {
        self.fleetCommander?.find(name)?.shoot(x: x, y: y)
    }
    
    func fleetStats() {
        print("FC kills: \(self.fleetCommander?.kills ?? 0)")
    }
    
    func stats(playerName name: String) {
        if let player = self.fleetCommander?.find(name) {
            print("[\(player.name)]: Kills - \(player.kills)")
        } else {
            print("No player with that name")
        }
    }
}
