//
//  Commander.swift
//  ATCompositePattern
//
//  Created by Dejan on 13/06/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

class Commander: Player
{
    var name: String
    var kills: Int {
        // A commander cannot kill stuff. His kill count is the agregate of his team.
        return self.players.reduce(0) { $0 + $1.kills }
    }
    
    private var players: [Player] = []
    
    init(name: String) {
        self.name = name
    }
    
    func move(x: Int, y: Int) {
        print("[\(self.name)]: I'm moving my team to [\(x):\(y)]")
        self.players.forEach { $0.move(x: x, y: y) }
    }
    
    func shoot(x: Int, y: Int) {
        print("[\(self.name)]: The guys will be killing as soon as we move to [\(x):\(y)]")
        self.players.forEach { $0.shoot(x: x, y: y) }
    }
    
    func addPlayer(_ player: Player) {
        guard self.hasPlayer(player) == false else { return }
        self.players.append(player)
    }
    
    func removePlayer(_ player: Player) {
        if let idx = self.players.index(where: { $0.name == player.name }) {
            self.players.remove(at: idx)
        }
    }
    
    func find(_ name: String) -> Player? {
        if name == self.name {
            return self
        } else {
            return self.players.filter { $0.find(name) != nil }.first
        }
    }
    
    private func hasPlayer(_ player: Player) -> Bool {
        return self.players.contains { $0.name == player.name }
    }
}
