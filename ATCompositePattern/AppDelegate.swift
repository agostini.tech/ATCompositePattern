//
//  AppDelegate.swift
//  ATCompositePattern
//
//  Created by Dejan on 13/06/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        testFleet()
        
        return true
    }
    
    let fleet = Fleet(name: "Golden Fleet")
    
    private func testFleet()
    {
//        fleet.fleetStats()
//
        fleet.moveFleet(x: 8, y: 7)
//        fleet.moveFleet(x: 3, y: 5)
//
//        fleet.fleetFire(x: 5, y: 2)
//        fleet.fleetFire(x: 3, y: 3)
//
//        fleet.move(playerName: "Redeemer[WC]", x: 4, y: 9)
//        fleet.fire(playerName: "TheGardener[SC]", x: 9, y: 9)
//        fleet.fire(playerName: "Avenger", x: 8, y: 8)
//
//        fleet.fleetStats()
//
//        fleet.stats(playerName: "Redeemer[WC]")
//        fleet.stats(playerName: "TheGardener[SC]")
//        fleet.stats(playerName: "Avenger")
    }
}

